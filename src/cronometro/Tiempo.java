package cronometro;



/**
 *
 * @author pablo
 */
public class Tiempo extends Thread {

    public int horas ,minutos,segundos,centiSegundos;

    public Tiempo() {      
        horas = 0;
        minutos = 0;
        segundos = 0;
        centiSegundos = 0;
    }
    
    public void run(){
        
        while(true)
            try {

                sleep(10);
                centiSegundos++;
                centiSegundoSegundo();
                segundosMinutos();
                minutosHoras();
                
            } catch (Exception e) {
            
            }
    }

    public void centiSegundoSegundo(){
        if(centiSegundos == 100){
            centiSegundos = 0;
            segundos++;
        }
    }

    public void segundosMinutos(){
        if(segundos == 60){
            segundos = 0;
            minutos++;
        }
    }
    
    public void minutosHoras(){
        if(minutos == 60){
            minutos = 0;
            horas++;
        }
    }
     
    @Override
    public String toString() {
        return  horas + ":" + minutos + ":" + segundos + ":" + centiSegundos;
    }
}
