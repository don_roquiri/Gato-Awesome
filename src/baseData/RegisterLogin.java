/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baseData;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;
import model.Usuario;

/**
 *
 * @author str4ng3-zone
 */
public class RegisterLogin implements CommonVars {

    public RegisterLogin() {
    }

    public static boolean CONEXION = false;

    private DateFormat fechaHora = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    public Connection conexion;

    /**
     * Crea la conexión usando las constantes
     */
    public Connection getConexion() {
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            System.out.println(e);
        }

        return con;
    }

    /**
     * verifica en la base de datos si el usuario esta en ella si la contraseña
     * es correcta y actualiza la ultima vez que se logeo
     *
     * @param userName userName used to compare in DB
     * @param passwordC password on char array 
     * @return
     */
    public boolean login(String userName, char[] passwordC) {

        PreparedStatement ps;
        ResultSet rs = null;
        Connection con = getConexion();
        boolean flag = true;
        String sql = "SELECT userName, email, password, countWinner, countLoss, id, IP, isConected FROM Gato.Usuarios WHERE userName = "
                   + "?";
        /**
         * Si userAdded es 1 entonces se encontro el usuario en la DB lo cual
         * puede proceder con todo el codigo
         */
        if (isUserAdded(userName) != 1) {
            JOptionPane.showMessageDialog(null, "No hay ningún usuario llamado así");
            flag = false;
        } else {
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, userName);
                rs = ps.executeQuery();
                String password = String.copyValueOf(passwordC);
                if (rs.next()) {
                    if (passwordSHA256(password).equals(rs.getString(3))) {
                        Usuario.USER.setUserName(userName);
                        Usuario.USER.setIP(getIp());
                        Usuario.USER.setPassword(password);
                    } else {
                        JOptionPane.showMessageDialog(null, "Contraseña invalida");
                        flag = false;
                    }
                }
                if (flag) {
                    //Update the last log and user conected
                    lastLog(ps, con, rs);
                    isConectedUser(ps, con, rs, true);
                }
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
                flag = false;
            }
        }
        return flag;
    }
    public boolean login(String userName, char[] passwordC, boolean a) {

        PreparedStatement ps;
        ResultSet rs = null;
        Connection con = getConexion();
        boolean flag = true;
        String sql = "SELECT userName, email, password, countWinner, countLoss, id, IP, isConected FROM Gato.Usuarios WHERE userName = "
                   + "?";
        /**
         * Si userAdded es 1 entonces se encontro el usuario en la DB lo cual
         * puede proceder con todo el codigo
         */
        if (isUserAdded(userName) != 1) {
            JOptionPane.showMessageDialog(null, "No hay ningún usuario llamado así");
            flag = false;
        } else {
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, userName);
                rs = ps.executeQuery();
                String password = String.copyValueOf(passwordC);
                if (rs.next()) {
                    if (passwordSHA256(password).equals(rs.getString(3))) {
                        Usuario.USER.setUserName(userName);
                        Usuario.USER.setIP(getIp());
                        Usuario.USER.setPassword(password);
                    } else {
                        JOptionPane.showMessageDialog(null, "Contraseña invalida");
                        flag = false;
                    }
                }
                if (flag) {
                    //Update the last log and user conected
                    lastLog(ps, con, rs);
                    isConectedUser(ps, con, rs, a);
                }
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
                flag = false;
            }
        }
        return flag;
    }
    /**
     * Registra un usuario, evitano que se repita el usuario en la base de datos
     * a su vez da una contraseña hashable en SHA-256
     *
     * @param userName
     * @param email
     * @param passwordC password on array of chars
     * @return
     */
    public boolean register(String userName, String email, char[] passwordC) {
        PreparedStatement ps;
        Connection con = getConexion();
        boolean flag = true;
        
        String sql = "INSERT INTO `Gato`.`Usuarios` (`userName`, `email`, `password`, `countWinner`, `countLoss`,`premiun`,`IP`) "
                   + "VALUES (?, ?, ?, ?, ?, ?, ?);";

        try {
            /**
             * Si es 0, entonces no hay ningún usuario/email llamado así en la
             * base de datos y puede ser insertado sin problemas
             */
            if (isUserAdded(userName) == 0 && isEmailAdded(email) == 0) {
                String password = String.copyValueOf(passwordC);
                ps = con.prepareStatement(sql);
                ps.setString(1, userName);

                if (!isValidEmailAddress(email)) {
                    JOptionPane.showMessageDialog(null, "Correo Electrónico Incorrecto");
                    flag = false;
                }
                ps.setString(2, email);
                ps.setString(3, passwordSHA256(password));
                ps.setInt(4, 0);
                ps.setInt(5, 0);
                ps.setInt(6, 0);
                ps.setString(7, encryptAES(getIp()));
                if (flag == true) {
                    ps.execute();
                }
                con.close();
            } else {
                JOptionPane.showMessageDialog(null, "Username o email replicado, escoja otro");
                flag = false;
            }
            return flag;//Si flag es true la insercción fue correcta 
        } catch (Exception e) {
            System.out.println(e);
            return false;//La insercción fallo
        }
    }

    private void lastLog(PreparedStatement ps, Connection con, ResultSet rs) {
        try {
            String sqlUpdate = "UPDATE Gato.Usuarios SET lastLog = ?, IP = ?"
                    + "WHERE id = ?";
            ps = con.prepareStatement(sqlUpdate);
            ps.setString(1, fechaHora.format(new Date()));
            ps.setString(2, encryptAES(getIp()));   //se actualiza el IP
            ps.setInt(3, rs.getInt(6)); //rs.getInt(6) == al id del usuario seleccionado   
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error en lastLog()");
        }
    }
    
    /**
     * 
     * @param ps
     * @param con
     * @param rs 
     */
    public void isConectedUser(PreparedStatement ps, Connection con, ResultSet rs, boolean conected) {
        try {
            String sqlUpdate = "UPDATE Gato.Usuarios SET isConected = ? "
                    + "WHERE id = ?";
            ps = con.prepareStatement(sqlUpdate);
            if(conected == true)
                ps.setInt(1, 1);
            else
                ps.setInt(1, 0);
            ps.setInt(2, rs.getInt(6)); //rs.getInt(6) == al id del usuario seleccionado   
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error en isConectedUser()");
        }
    }
    
    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    private int isUserAdded(String userName) {
        PreparedStatement ps;
        ResultSet rs = null;
        Connection con = getConexion();
        String sql = "SELECT COUNT(userName) FROM Gato.Usuarios WHERE userName = "
                + "?";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, userName);
            rs = ps.executeQuery();
            if (rs.next()) {
                rs.getInt(1);
                return rs.getInt(1);//regresa las coincidencias de usuarios con ese username
            } else {
                return 1;//Ocurrio un error
            }

        } catch (Exception e) {
            System.out.println("Error en userAdded");
            System.out.println(e);
            return 1;//Ocurrio un error
        }
    }

    private int isEmailAdded(String email) {
        PreparedStatement ps;
        ResultSet rs = null;
        Connection con = getConexion();
        String sql = "SELECT COUNT(email) FROM Gato.Usuarios WHERE email = "
                + "?";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                rs.getInt(1);
                return rs.getInt(1);//regresa las coincidencias de usuarios con ese username
            } else {
                return 1;//Ocurrio un error
            }

        } catch (Exception e) {
            System.out.println("Error en emailAdded");
            System.out.println(e);
            return 1;//Ocurrio un error
        }
    }

    /**
     * Since password isn't complety needed to decrypt its hashed, there's just
     * one way to get the password, yes brute force
     *
     * @param password
     * @return
     */
    private String passwordSHA256(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

        byte[] hash = md.digest(password.getBytes());
        StringBuffer sb = new StringBuffer();

        for (byte b : hash) {
            sb.append(String.format("%02x", b));
        }
        System.out.println("Password on SHA256: " + sb.toString());
        return sb.toString();
    }
    
    private final String KEY = "MaybeIfIKissYour"; // 128 bit key Maybe song Janis Joplin 
    
    private String encryptAES(String secretToEncrypt) {
        // Create key and cipher
        Key aesKey = new SecretKeySpec(KEY.getBytes(), "AES");
        byte[] encrypted = null;
        /**
         * The key must be saved on the data base to decrypt the data, this will
         * be useful for the IP
         * Since i can't use SHA 256 method, cus i will need make a bruteForce method
         * basically a loop to generate random hashes and compare them.
         */
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            encrypted = cipher.doFinal(secretToEncrypt.getBytes());
            // decrypt the text
        } catch (Exception e) {
            
        }
        
        if(encrypted != null)
            return new String(encrypted);
        else
            return null; //error
    }
    /**
     * select the user from DB and get the 
     * encripted secret you need to work
     * @param secretMessage
     * @param userName 
     */
    String decryptAES(String secretMessage){
        String decrypted = null;
        try{
            Key aesKey = new SecretKeySpec(KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            decrypted = new String(cipher.doFinal(secretMessage.getBytes()));
            System.err.println(decrypted);   
        }catch(Exception e){
            
        }
        if(decrypted != null)
            return decrypted;
        else
            return null;
    }
    
    /**
     * This works on all OS like OSX/Windows/GNULinux
     * @return IP 
     */
    public String getIp() {
        try {
           Socket socket = new Socket();
           socket.connect(new InetSocketAddress("google.com", 80));
           Usuario.USER.setIP(socket.getLocalAddress().toString());
           socket.close(); 
           return Usuario.USER.getIP();
        } catch (Exception e) {
            Logger.getLogger(chat.Client.class.getName()).log(Level.WARNING, "IP DOESNT FOUND MAYBE INTERNET ERROR", e);
            return null;
        }
    }   
    
}
