/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baseData;

import chat.ChatGUI;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import model.MessagePackage;
import model.Usuario;

;

/**
 *
 * @author str4ng3r
 */
public class Client implements CommonVars, Runnable {
    static Thread th;

    public Client() {
        th = new Thread(this);
        th.start();
    }

    public static void outMessage(String message, String userName, String IP) {
        MessagePackage outPack = new MessagePackage();
        try {
            outPack.setIP(IP);
            outPack.setMessage(message);
            outPack.setUserName(userName);
            Socket client = new Socket(Usuario.USER.getIP(), PORT);
            ObjectOutputStream mensaje = new ObjectOutputStream(client.getOutputStream());
            mensaje.writeObject(outPack);
            
            mensaje.flush();
            mensaje.close();
            client.close();
        } catch (Exception e) {
        }
    }
    /**
     * I am trying to figure out how to fix this
     * this will be running all the whole time
     * because its running on a thread in a infinite loop
     * the idea its trying to get the messages without a server
     */
    private void readyForRead() {
        MessagePackage pack;
        try {
            ServerSocket servidorCliente = new ServerSocket(PORT);
            Socket cliente;
            while (true) {     
                cliente = servidorCliente.accept();
                ObjectInputStream inputStream = new ObjectInputStream(cliente.getInputStream());
                pack = (MessagePackage)inputStream.readObject();
                ChatGUI.getMessage(pack.getUserName(), pack.getMessage());
                inputStream.close();
            }

        } catch (Exception e) {
        }

    }
    
    public static void stop(){
        if(th.isAlive()){
            th.stop();
        }
    }
    
    @Override
    public void run() {
        while(true){
            readyForRead();
        }
    }

}
