/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author str4ng3-zone 
 * Esta clase sirve para crear rapidamente una barra de
 * menu sin tanto codigo y poner los action listenner en el main de la clase
 * medianate un get
 */
public class MenuR {

    private JMenuBar barraxd;
    private JMenuItem items[];

    public MenuR(String nombreMenus[], String[] nombresItems) {
        byte i;
        JMenu menus[];
        barraxd = new JMenuBar();
        menus = new JMenu[nombreMenus.length];
        items = new JMenuItem[nombresItems.length];

        for (i = 0; i < nombreMenus.length; i++) {
            menus[i] = new JMenu(nombreMenus[i]);
        }

        byte j = 0;
        for (i = 0; i < nombresItems.length; i++) {
            if (nombresItems[i].equals("separar menu")) {
                j++;
            } else if (nombresItems[i].equals("separador")) {
                menus[j].addSeparator();
            } else {
                items[i] = new JMenuItem(nombresItems[i]);
                menus[j].add(items[i]);
            }
        }

        for (i = 0; i < menus.length; i++) {
            barraxd.add(menus[i]);
        }

    }

    /**
     * @return the items para poner eventos y hacerlo chido
     */
    public JMenuItem[] getItems() {
        return items;
    }

    /**
     * @return the barraxd para colocarla barra
     */
    public JMenuBar getBarra() {
        return barraxd;
    }

}
