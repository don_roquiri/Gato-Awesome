/**
 *   ██ ██████████ ██   ██████                 ██████████     ██       ██████
 *  ██ ░░░░░██░░░ ░██  ██░░░░██        █      ░░░░░██░░░     ████     ██░░░░██
 * ██      ░██    ░██ ██    ░░        ░█          ░██       ██░░██   ██    ░░
 *░██      ░██    ░██░██           █████████      ░██      ██  ░░██ ░██
 *░██      ░██    ░██░██          ░░░░░█░░░       ░██     ██████████░██
 *░░██     ░██    ░██░░██    ██       ░█          ░██    ░██░░░░░░██░░██    ██
 * ░░██    ░██    ░██ ░░██████        ░           ░██    ░██     ░██ ░░██████
 *  ░░     ░░     ░░   ░░░░░░                     ░░     ░░      ░░   ░░░░░░
 *              ██████████   ███████   ████████ ██       ██      ████
 *      █      ░░░░░██░░░   ██░░░░░██ ░██░░░░░ ░░██    ██░ ██   █░░░ █
 *     ░█          ░██     ██     ░░██░██       ░░██ ██   ░░ ██░    ░█   ██████
 *  █████████      ░██    ░██      ░██░███████   ░██░░      ░░    ███   ░░░░░░
 * ░░░░░█░░░       ░██    ░██      ░██░██░░░░    ░██             █░░     ██████
 *     ░█          ░██    ░░██     ██ ░██        ██             █       ░░░░░░
 *     ░           ░██     ░░███████  ░████████ ██             ░██████
 *                 ░░       ░░░░░░░   ░░░░░░░░ ░░              ░░░░░░
 *     ██     ██       ██ ████████  ████████   ███████   ████     ████ ████████
 *    ████   ░██      ░██░██░░░░░  ██░░░░░░   ██░░░░░██ ░██░██   ██░██░██░░░░░
 *   ██░░██  ░██   █  ░██░██      ░██        ██     ░░██░██░░██ ██ ░██░██
 *  ██  ░░██ ░██  ███ ░██░███████ ░█████████░██      ░██░██ ░░███  ░██░███████
 * ██████████░██ ██░██░██░██░░░░  ░░░░░░░░██░██      ░██░██  ░░█   ░██░██░░░░
 *░██░░░░░░██░████ ░░████░██             ░██░░██     ██ ░██   ░    ░██░██
 *░██     ░██░██░   ░░░██░████████ ████████  ░░███████  ░██        ░██░████████
 *░░      ░░ ░░       ░░ ░░░░░░░░ ░░░░░░░░    ░░░░░░░   ░░         ░░ ░░░░░░░░
 */

package gato;

import baseData.RegisterLogin;
import chat.ChatGUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import model.Usuario;

/**
 *
 * @author str4ng3-zone
 */
public class FrontEnd extends JFrame implements ActionListener {

    static Login frmLoginRegister = null;
    static GameSetup frmSetup = null;
    static TableLogGame frmLogCat = null;
    static ChatGUI frmChat = null;
    static byte tipoDeJuego = 0;

    JPanel mainPanel = new JPanel(new BorderLayout());
    JMenuItem items[];
    MenuR menus;
    JPanel panelPrincipal;

    public static boolean turno = true;
    static boolean primeraVez = true;
    static boolean juegoTerminado = false;

    static CuadroGrandeConSusCuadritos cuadro[] = new CuadroGrandeConSusCuadritos[9];

    byte i, j;

    public FrontEnd() {
        super("(Tic + Tac + Toe)² = Awesome");

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        byte id = 0;
        GridBagConstraints coordenadas = new GridBagConstraints();
        panelPrincipal = new JPanel(new GridBagLayout());
        panelPrincipal.setBackground(Color.black);

        for (i = 0; i < 3; i++) {
            coordenadas.gridy = i;
            for (j = 0; j < 3; j++) {
                coordenadas.gridx = j;
                cuadro[id] = new CuadroGrandeConSusCuadritos(id, Color.white);
                cuadro[id].getCuadroConCuadritos().setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
                cuadro[id].getCuadroConCuadritos().setBackground(Color.black);//genera las "barras negras"
                panelPrincipal.add(cuadro[id].getCuadroConCuadritos(), coordenadas);
                id++;
            }
        }

        crearBarraDeMenu();
        items = menus.getItems();

        for (i = 0; i < items.length; i++) {
            if (items[i] != null) {
                items[i].addActionListener(this);
            }
        }

        frmChat = new ChatGUI();
        frmLogCat = new TableLogGame();

        mainPanel.add(panelPrincipal, BorderLayout.CENTER);
        mainPanel.add(frmChat, BorderLayout.EAST);
        mainPanel.add(frmLogCat, BorderLayout.SOUTH);

        setJMenuBar(menus.getBarra());
        setContentPane(mainPanel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(595, 425));
        setSize(575, 405);
        setLocationRelativeTo(null);
        setVisible(true);

    }

    /**
     * Al salir del juego debe de cambiar de estado en la base de datos de 1 a 0
     *
     * @param evt
     */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {
        RegisterLogin goodBye = new RegisterLogin();
        if (Usuario.USER.getUserName() != null) {
            goodBye.login(Usuario.USER.getUserName(), Usuario.USER.getPassword().toCharArray(), false);
        }
        JOptionPane.showMessageDialog(null, "GRACIAS POR JUGAR");
        dispose();
    }

    private void crearBarraDeMenu() {
        menus = new MenuR(new String[]{"Partida", "Online", "Ayuda"},
                new String[]{"Nuevo Juego",
                    "separar menu",
                    "Registrar/Login", "separador",
                    "Configuración",
                    "separar menu",
                    "¿Como jugar?", "separador",
                    "Acerca de mi? :D!"});
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        if (ae.getSource() instanceof JMenuItem) {
            if (ae.getSource() == items[0]) {
                switch (JOptionPane.showConfirmDialog(null, "¿Estas seguro de empezar un nuevo juego?", "NUEVO JUEGO", JOptionPane.YES_NO_OPTION)) {
                    case 0:
                        for (i = 0; i < 9; i++) {
                            cuadro[i].nuevoJuego();
                        }
                        break;
                    case 1:
                        System.out.println("No hagas nada");
                        break;
                }
            }

            //No abrir si ya habiá sido abierta
            if (ae.getSource() == items[2]) {
                if (frmLoginRegister == null) {
                    frmLoginRegister = new Login();
                }
            }

            if (ae.getSource() == items[4]) {
                if (frmSetup == null) {
                    frmSetup = new GameSetup();
                }
            }

            if (ae.getSource() == items[6]) {
                JOptionPane.showMessageDialog(null, "La finalidad de este juego es ganar como en un gato común y corriente\n"
                        + "Solo que al cuadrado :P");
            }

            if (ae.getSource() == items[8]) {
                JOptionPane.showMessageDialog(null, "Pablo Eduardo Martinez\n"
                        + "¿Estudiante? a veces no lo se\n"
                        + "Github @Str4ng3r\nGitlab @don_roquiri\n"
                        + "Disfruta de mi juego, todo un fractal!");
            }
        }
    }

    static int idF;

    /**
     * Solo lo utilizo como condicción para saber si seleccionar un cuadro con
     * el color random o no si es true entonces pinta el jpanel de colores
     * random, en caso contrario deja pintado todo de negro
     *
     * @param boolean a
     */
    static void seleccionarId(boolean a) {
        for (byte i = 0; i < 9; i++) {
            cuadro[i].getCuadroConCuadritos().setBackground(Color.black);
        }
        if (a == true) {
            drawSquares(idF);
        }
    }

    static void winner(int i, int j, int k) {
        seleccionarId(false);

        Timer t = new Timer();
        TimerTask task = null;
        task = new TimerTask() {
            @Override
            public void run() {
                drawSquares(i);
            }
        };
        t.schedule(task, 700);

        task = new TimerTask() {
            @Override
            public void run() {
                drawSquares(j);
            }
        };
        t.schedule(task, 1200);

        task = new TimerTask() {
            @Override
            public void run() {
                drawSquares(k);
            }
        };
        t.schedule(task, 1600);
    }

    private static void drawSquares(int i) {
        cuadro[i].getCuadroConCuadritos().setBackground(
                new Color((int) (Math.random() * 105) + 150,
                          (int) (Math.random() * 155) + 100,
                          (int) (Math.random() * 205) + 50));//colores menos grises
    }

    /**
     * Verifica si ya el juego ah concluido básicamente el mismo metodo del
     * backEndGato pero modificado para verificar en los cuadros y tambien si es
     * del mismo signo
     */
    static void isWinner() {
        if (cuadro[0].backEndGato.winner && cuadro[1].backEndGato.winner && cuadro[2].backEndGato.winner
                && (cuadro[0].backEndGato.whichOne == cuadro[1].backEndGato.whichOne)
                && (cuadro[2].backEndGato.whichOne == cuadro[1].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(0, 1, 2);
        }
        if (cuadro[3].backEndGato.winner && cuadro[4].backEndGato.winner && cuadro[5].backEndGato.winner
                && (cuadro[3].backEndGato.whichOne == cuadro[4].backEndGato.whichOne)
                && (cuadro[4].backEndGato.whichOne == cuadro[5].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(3, 4, 5);
        }
        if (cuadro[6].backEndGato.winner && cuadro[7].backEndGato.winner && cuadro[8].backEndGato.winner
                && (cuadro[6].backEndGato.whichOne == cuadro[7].backEndGato.whichOne)
                && (cuadro[7].backEndGato.whichOne == cuadro[8].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(6, 7, 8);
        }
        if (cuadro[0].backEndGato.winner && cuadro[3].backEndGato.winner && cuadro[6].backEndGato.winner
                && (cuadro[0].backEndGato.whichOne == cuadro[3].backEndGato.whichOne)
                && (cuadro[3].backEndGato.whichOne == cuadro[6].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(0, 3, 6);
        }
        if (cuadro[1].backEndGato.winner && cuadro[4].backEndGato.winner && cuadro[7].backEndGato.winner
                && (cuadro[1].backEndGato.whichOne == cuadro[4].backEndGato.whichOne)
                && (cuadro[7].backEndGato.whichOne == cuadro[1].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(1, 4, 7);
        }
        if (cuadro[2].backEndGato.winner && cuadro[5].backEndGato.winner && cuadro[8].backEndGato.winner
                && (cuadro[2].backEndGato.whichOne == cuadro[5].backEndGato.whichOne)
                && (cuadro[5].backEndGato.whichOne == cuadro[8].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(2, 5, 8);
        }
        if (cuadro[0].backEndGato.winner && cuadro[4].backEndGato.winner && cuadro[8].backEndGato.winner
                && (cuadro[0].backEndGato.whichOne == cuadro[4].backEndGato.whichOne)
                && (cuadro[4].backEndGato.whichOne == cuadro[8].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(0, 4, 8);
        }
        if (cuadro[2].backEndGato.winner && cuadro[4].backEndGato.winner && cuadro[6].backEndGato.winner
                && (cuadro[2].backEndGato.whichOne == cuadro[4].backEndGato.whichOne)
                && (cuadro[4].backEndGato.whichOne == cuadro[6].backEndGato.whichOne)) {
            juegoTerminado = true;
            winner(2, 4, 6);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new FrontEnd());
    }
}
