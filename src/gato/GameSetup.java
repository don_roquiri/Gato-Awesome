/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import static baseData.CommonVars.PASSWORD;
import static baseData.CommonVars.URL;
import static baseData.CommonVars.USERNAME;
import baseData.RegisterLogin;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class GameSetup extends JFrame implements ActionListener {

    ButtonGroup grupo = new ButtonGroup(),
            grupo2 = new ButtonGroup();

    JRadioButton playerVsPlayer, playerVsPc, online, offline;
    JLabel gameSetup, texto0, texto1;
    JPanel rightPanel = new JPanel();
    JPanel panel = new JPanel();
    JButton ok0, ok1;
    JTextField txtPort, txtIp;
    JTable tableUsers;
    private ArrayList <UsersFromDB> usersFromDB = new ArrayList<>(); 
    private DefaultTableModel mdl = new DefaultTableModel();
    
    String IP = "", port = "";
    /**
     * tipoDeJuego Offline = 0 
     * player1 vs player 2 Offline = 1 
     * player1 vs Player 2 Online = 2
     */
    public GameSetup() {
        super("GAME SETUP");
        setLayout(new BorderLayout());
        tableUsers = new JTable(new DefaultTableModel(
                new String[][]{
                    {}
                }
                ,
                new Object [] {
                    "User Name", "Count Win", "Count Loss"
                }
        ));
        
        FrontEnd.tipoDeJuego = 0;

        ok0 = new JButton("OK");
        ok1 = new JButton("BUSCAR USUARIOS");
        online = new JRadioButton("Online");
        offline = new JRadioButton("Offline");
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        online.addActionListener(this);
        offline.addActionListener(this);
        offline.setSelected(true);

        txtPort = new JTextField(10);
        txtIp = new JTextField(10);
        ImageIcon icon = new ImageIcon(getClass().getResource("../Drawables/settings.png"));
        setIconImage(icon.getImage());

        gameSetup = new JLabel("       GAME SETUP                                    ", icon, SwingConstants.CENTER);
        gameSetup.setOpaque(true);
        gameSetup.setBackground(new Color(9, 210, 97));

        icon = new ImageIcon(getClass().getResource("../Drawables/nowifi.png"));
        texto0 = new JLabel("       OFFLINE                                           ", icon, SwingConstants.CENTER);
        texto0.setOpaque(true);
        texto0.setBackground(new Color(9, 210, 97));

        playerVsPc = new JRadioButton("Player vs PC");
        playerVsPlayer = new JRadioButton("Player vs Player");

        playerVsPlayer.setSelected(true);

        icon = new ImageIcon(getClass().getResource("../Drawables/world.png"));
        texto1 = new JLabel("       ONLINE                                           ", icon, SwingConstants.CENTER);
        texto1.setOpaque(true);
        texto1.setBackground(new Color(9, 210, 97));
        
        grupo.add(playerVsPc);
        grupo.add(playerVsPlayer);
        grupo2.add(online);
        grupo2.add(offline);

        ok0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonClicked(e);
            }
        });

        ok1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonClickedOnline(e);
            }
        });

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        rightPanel.setLayout(new BorderLayout());
        //Añadimos los paneles como debe ser
        add(panel, BorderLayout.WEST);
        //Añadimos el otro panel como debe ser ell panel del online
        
        add(rightPanel, BorderLayout.EAST);
        addComponents();
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addComponents() {
        txtIp.setEnabled(false);
        txtPort.setEnabled(false);
        ok1.setEnabled(false);
        panel.add(gameSetup);
        panel.add(online);
        panel.add(offline);
        panel.add(texto0);
        panel.add(playerVsPc);
        panel.add(playerVsPlayer);
        panel.add(ok0);
        //AGREGAMOS EL PANEL DEL ONLINE
        rightPanel.add(texto1, BorderLayout.NORTH);
        rightPanel.add(new JScrollPane(tableUsers), BorderLayout.CENTER);
        
    }

    private void buttonClicked(ActionEvent e) {
        if (playerVsPlayer.isSelected()) {
            FrontEnd.tipoDeJuego = 0;
        } else {
            FrontEnd.tipoDeJuego = 1;
        }
        dispose();
        FrontEnd.frmSetup = null;
    }
    
     
    private void buttonClickedOnline(ActionEvent e) {
        String ip = "", userName = "";
        if(getSelectedItem()!=0){
           ip = usersFromDB.get(getSelectedItem()).ip;
           userName  = usersFromDB.get(getSelectedItem()).userName;
        }else
            JOptionPane.showMessageDialog(null ,"SELECCIONE UN USARIO\n Con el que quiera jugar");
    }

    private void formWindowClosing(WindowEvent evt) {
        dispose();
        FrontEnd.frmSetup = null;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        if (evento == online) {
            playerVsPlayer.setEnabled(false);
            playerVsPc.setEnabled(false);
            ok0.setEnabled(false);
            
            if(RegisterLogin.CONEXION == true){//Entonces se ah conectado a la base de datos
                tableUsers.setEnabled(true);
                showUsersConected();
            }else{
                tableUsers.setEnabled(false);
                JOptionPane.showMessageDialog(null, "Necesita logearse primeramente para utilizar esta opción");
            }
            ok1.setEnabled(true);
        } else {
            playerVsPlayer.setEnabled(true);
            playerVsPc.setEnabled(true);
            ok0.setEnabled(true);
            txtIp.setEnabled(false);
            txtPort.setEnabled(false);
            ok1.setEnabled(false);
        }

    }
    
    public int getSelectedItem(){
        return tableUsers.getSelectedColumnCount();//Retornar la columna seleccionada
    }
    
    public void showUsersConected() {
        getListUsers();
        Object[][] data = new Object[usersFromDB.size()][3];
        
        for (int i = 0; i < usersFromDB.size(); i++) {
            data[i][0] = usersFromDB.get(i).userName;
            data[i][1] = usersFromDB.get(i).countWinner;
            data[i][2] = usersFromDB.get(i).countLoss;
        }
        
        tableUsers.setModel(new javax.swing.table.DefaultTableModel(
                data
                ,
                new String [] {
                    "User Name", "Count Win", "Count Loss"
                }
        ));
    }

    Connection conexion;

    /**
     * Crea la conexión usando las constantes
     */
    private Connection getConexion() {
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            System.out.println(e);
        }

        return con;
    }
    /**
     * Get the list of all the users in the DB
     * @return flag if have an issue with internet conection
     * you cannot get the user list oder by userName
     */
    private void getListUsers() {

        PreparedStatement ps;
        ResultSet rs = null;
        Connection con = getConexion();
        String sql = "SELECT userName, ip, countWinner, countLoss, IP FROM Gato.Usuarios WHERE isConected = 1 ORDER BY userName";

            try {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()) {
                    usersFromDB.add(new UsersFromDB(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4)));
                }
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
            }
    }      
}

class UsersFromDB{
    String userName, ip;
    int countWinner, countLoss;

    public UsersFromDB(String userName, String ip, int countWinner, int countLoss) {
        this.userName = userName;
        this.ip = ip;
        this.countWinner = countWinner;
        this.countLoss = countLoss;
    }

    @Override
    public String toString() {
        return "UsersFromDB{" + "userName=" + userName + ", ip=" + ip + ", countWinner=" + countWinner + ", countLoss=" + countLoss + '}';
    }
    
}
