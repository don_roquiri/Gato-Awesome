/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;


/**
 *
 * @author str4ng3r
 */
public class Gato {

    char whichOne;
    boolean winner = false;
    byte[][] tablero = new byte[3][3];
    byte movimientos = 0;

    Gato() {
        for (byte i = 0; i < 3; i++) {
            for (byte j = 0; j < 3; j++) {
                tablero[i][j] = 0;
            }
        }  
    }
    
    void newGame(){
        winner = false;
        movimientos = 0;
        for (byte i = 0; i < 3; i++) {
            for (byte j = 0; j < 3; j++) {
                tablero[i][j] = 0;
            }
        }          
    }
    
    boolean isWinner(int x) {
        winner = ganaPorColumna(x);
        if (winner == false) {
            winner = ganaPorRenglon(x);
        }
        if (winner == false) {
            winner = ganaPorDiagonales(x);
        }
        if (winner == true) {
            whichOne = x == 1 ? 'X' : 'O';
        }
        return winner;
    }

    private boolean ganaPorRenglon(int caracter) {
        for (byte[] tablero1 : tablero) {
            if (tablero1[0] == caracter && tablero1[1] == caracter && tablero1[2] == caracter) {
                return true;
            }
        }
        return false;
    }

    private boolean ganaPorColumna(int caracter) {
        for (int i = 0; i < tablero.length; i++) {
            if (tablero[0][i] == caracter && tablero[1][i] == caracter && tablero[2][i] == caracter) {
                return true;
            }
        }
        return false;
    }

    private boolean ganaPorDiagonales(int caracter) {
        // Busca ganador en la columna de izquierda a derecha
        if (tablero[0][0] == caracter && tablero[1][1] == caracter && tablero[2][2] == caracter) {
            return true;
        }
        if (tablero[0][2] == caracter && tablero[1][1] == caracter && tablero[2][0] == caracter) {
            return true;
        }

        return false;
    }

    /**
     * este metodo se utiliza cuando juegas contra la PC
     *
     * @param x se utiliza para lo coordenada en x
     * @param y se utiliza para la cordenada en y
     * @return true entonces puedes jugar ahi si es false entonces no puedes por
     * que ya has jugado ahí
     */
    boolean turno(int x, int y) {
        if (tablero[x][y] == 0) {
            tablero[x][y] = 1;
            movimientos++;
            return true;
        } else {
            return false;
        }
    }
    
    void turnoVSPc(int x, int y) {
            tablero[x][y] = 1;
            movimientos++;
            System.out.println("PLAYER");
            imprimirTablero();
    }    

    boolean verificarCasilla(int x, int y) {
        return tablero[x][y] == 0;
    }

    void imprimirTablero(){
        System.out.println("Tablero Gato Backend");
        for (byte i = 0; i < 3; i++) {
            for (byte j = 0; j < 3; j++) {
                System.out.print(tablero[i][j]+" ");
            }
        }
    }
    
    /**
     * Este otro se utiliza cuando juegas contra otra persona
     *
     * @param x coord
     * @param y coord
     * @param c X/O
     */
    void turno(int x, int y, char c) {
        if (tablero[x][y] == 0) {
            if (c == 'X') {
                tablero[x][y] = 1;
            } else {
                tablero[x][y] = 2;
            }
            movimientos++;
            imprimirTablero();
        }
    }
    
    private byte x = 0, y = 0;
    
    /**
     * Lo mismo que el turno
     * solo que esta vez la computadora juega
     * si ya se habia jugado en esa casilla
     * entonces entra en un metodo recursivo
     * el cual genera otra vez números random
     * y comprueba si se puede jugar ahí
     */
    void turnoComputadora() {
        do {
            x = (byte) (Math.random() * 3);
            y = (byte) (Math.random() * 3);
        } while (verificarCasilla(y, x) == false);
        System.out.println("X " + x + "Y " + y);
        tablero[x][y] = 2;
        imprimirTablero();
        movimientos++;
    }
    void turnoVsPc(){
        
    }
    public byte getX() {
        return x;
    }

    public byte getY() {
        return y;
    }
 
}
