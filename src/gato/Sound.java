package gato;

import java.io.File;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.swing.ImageIcon;
 
public class Sound {
    
    String root; 

    public Sound(String root) {
        this.root = root;
    }
    
    Sound()
    {
        root = "" +  new ImageIcon(getClass().getResource("../").getPath());
        Clip sound = getSound();
	playSound(sound);
    }
	public Clip getSound()
        {
		try
		{
                        System.out.println(root);
                        root += "/Sounds/welcome.waw";
                        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(
                                new File("/home/str4ng3r/NetBeansProjects/Gato/src/Sounds/welcome.wav"));
			AudioFormat format = audioInputStream.getFormat();
			DataLine.Info info = new DataLine.Info(Clip.class, format);
			Clip sound = (Clip)AudioSystem.getLine(info);
			sound.open(audioInputStream);
			return sound;
		}
		catch(Exception e)
		{
                    System.out.println("e: "+e);
			return null;
		}
	}
	public void playSound(Clip clip)
	{
		clip.stop();
		clip.setFramePosition(0);
		clip.start();
	}	
        
}