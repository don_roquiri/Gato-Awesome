/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author str4ng3r
 */
public class TableLogGame extends JPanel {
    
    private JTable log;
    private DefaultTableModel mdl = new DefaultTableModel();
    
    public TableLogGame() {
        
        super();
        log = new JTable(mdl);
        mdl.addColumn("CUADRO");
        mdl.addColumn(" X ");
        mdl.addColumn(" Y ");
        mdl.addColumn(" TURNO ");
        log.setPreferredScrollableViewportSize(new Dimension(555, 50));
        log.setBorder(BorderFactory.createEmptyBorder(2, 0, 3, 0));
        log.getTableHeader().setBackground(new Color(9 , 210, 97));
        log.setBackground(Color.BLACK);
        log.setForeground(Color.white);
        log.setEnabled(false);
        log.setOpaque(true);
        log.setDragEnabled(true);
        
        TitledBorder bord = new TitledBorder("Log del Juego");
        bord.setTitleColor(Color.white);
        bord.setTitlePosition(TitledBorder.TOP);
        
        setBorder(bord);
        
        add(new JScrollPane(log), BorderLayout.CENTER);
        setBackground(Color.BLACK);
        
    }
    
    void setCoords(byte id, byte x, byte y, char c){
        String coords [] = new String[4];
        
        coords[0] = "" + (id + 1);
        coords[1] = "" + (x + 1);
        coords[2] = "" + (y + 1);
        coords[3] = "" + c;
        
        mdl.addRow(coords);

    }

    void clearLogGame(){
        if(mdl.getRowCount() > 0){
        for (byte i = (byte) (mdl.getRowCount() - 1); i > -1; i--) 
            mdl.removeRow(i);
        }
    }
    
}
