/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import gato.FrontEnd;
import gato.SeleccionarId;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Esta clase crea los cuadros del gato utilizando un gridBagLayout la variable
 * de tipo booleano equisCirculo corresponde a como quieran jugar Basicamente
 * aqui sucede toda la magia, esta clase tiene todos los atributos necesarios
 *
 */
public class CuadroGrandeConSusCuadritos implements ActionListener {

    boolean gatitoGanado = false;
    Gato backEndGato;
    private JPanel cuadroConCuadritos;
    byte id; //me permite conocer cual cuadro fue seleccionado
    private JButton cuadritos[][] = new JButton[3][3];
    byte i, j;

    CuadroGrandeConSusCuadritos(byte id, Color color) {
        this.id = id;
        backEndGato = new Gato();
        GridBagConstraints config1 = new GridBagConstraints();
        cuadroConCuadritos = new JPanel(new GridBagLayout());
        for (i = 0; i < 3; i++) {
            config1.gridx = i;
            for (j = 0; j < 3; j++) {
                config1.gridy = j;
                cuadritos[i][j] = new JButton();
                cuadritos[i][j].setFont(new Font("Consolas", Font.PLAIN, 12));
                cuadritos[i][j].setText(" ");
                cuadritos[i][j].setSize(new Dimension(30, 30)); //Esto todavia no funciona, no tengo idea aún
                cuadritos[i][j].setBackground(color);
                cuadritos[i][j].setForeground(Color.BLACK);
                cuadroConCuadritos.add(cuadritos[i][j], config1);
            }
        }
        asignarEventos();
    }

    private void asignarEventos() {
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                cuadritos[i][j].addActionListener(this);
            }
        }
    }

    /**
     * @return the Cuadro Grande este cuadro contiene cuadritos chiquitos :3
     */
    public JPanel getCuadroConCuadritos() {
        return cuadroConCuadritos;
    }

    SeleccionarId cuadroDondeJugar = new SeleccionarId();
    byte x, y;
    char c;

    /**
     * Evento de los botones, se utiliza para los turnos y conocer el id que
     * selecciona al gato
     *
     * @param ae
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        c = 'X';
        switch (FrontEnd.tipoDeJuego) {

            case 0:
                //Jugador contra jugador
                if (FrontEnd.juegoTerminado == false) {

                    boolean exito = false;
                    c = FrontEnd.turno == true ? 'X' : 'O';

                    if (ae.getSource() instanceof JButton) {
                        for (x = 0; x < 3; x++) {
                            for (y = 0; y < 3; y++) {

                                if (FrontEnd.primeraVez == true) {

                                    if (ae.getSource() == cuadritos[x][y]) {
                                        backEndGato.turno(x, y, c);
                                        cuadritos[x][y].setText("" + c);
                                        System.out.println("id = " + id + " x = " + x + " y = " + y);
                                        FrontEnd.frmLogCat.setCoords(id, x, y, c);
                                        FrontEnd.idF = cuadroDondeJugar.selecionarId(x, y);
                                        FrontEnd.seleccionarId(true);
                                        FrontEnd.primeraVez = false;
                                        exito = true;
                                        break;
                                    }

                                } else if (ae.getSource() == cuadritos[x][y] && id == FrontEnd.idF && backEndGato.verificarCasilla(x, y) && gatitoGanado == false) {
                                    backEndGato.turno(x, y, c);
                                    cuadritos[x][y].setText("" + c);
                                    System.out.println("id = " + id + " x = " + x + " y = " + y);
                                    FrontEnd.frmLogCat.setCoords(id, x, y, c);
                                    FrontEnd.idF = cuadroDondeJugar.selecionarId(x, y);
                                    FrontEnd.seleccionarId(true);
                                    exito = true;
                                    break;
                                } else if (x == 2 && y == 2) {
                                    JOptionPane.showMessageDialog(null, "No puedes jugar ahí");
                                }

                            }
                            if (exito == true) {
                                break;
                            }
                        }
                        //Fin del for concatenado
                        if (exito) {
                            turnoVerificarSiGanoCuadrito();
                        }
                        //Verifica si ya gano
                        FrontEnd.isWinner();
                        if (FrontEnd.juegoTerminado == true) {
                            JOptionPane.showMessageDialog(null, "Felicidades has ganado");
                        } else {
                            selecionarGatitoMenosMovi();
                        }
                    }
                    break;
                }
                break;

            case 1:
                //Jugador vs computadora
                if (FrontEnd.juegoTerminado == false) {
                    playerVsPc(ae, c);
                } else {
                    JOptionPane.showMessageDialog(null, "Felicidades has ganado");
                }
                break;
            case 2:
                playerOnline();
                break;
        }
    }

    private void playerVsPc(ActionEvent ae, char c) {
        boolean exito = false;
        c = FrontEnd.turno == true ? 'X' : 'O';

        for (x = 0; x < 3; x++) {
            for (y = 0; y < 3; y++) {
                if (FrontEnd.primeraVez == true) {

                    if (ae.getSource() == cuadritos[x][y]) {
                        System.out.println("Primer turno");
                        backEndGato.turnoVSPc(x, y);
                        cuadritos[x][y].setText("" + c);
                        FrontEnd.frmLogCat.setCoords(id, x, y, c);
                        FrontEnd.idF = cuadroDondeJugar.selecionarId(x, y);
                        FrontEnd.seleccionarId(true);
                        FrontEnd.primeraVez = false;
                        exito = true;
                        break;
                    }

                } else if (ae.getSource() == cuadritos[x][y] && id == FrontEnd.idF && backEndGato.verificarCasilla(x, y)) {
                    backEndGato.turnoVSPc(x, y);
                    cuadritos[x][y].setText("" + c);
                    FrontEnd.frmLogCat.setCoords(id, x, y, c);
                    FrontEnd.idF = cuadroDondeJugar.selecionarId(x, y);
                    FrontEnd.seleccionarId(true);
                    exito = true;
                    break;
                } else if (x == 2 && y == 2 && exito == false) {
                    JOptionPane.showMessageDialog(null, "No puedes tirar ahí");
                }
            }
        }

        FrontEnd.isWinner();
        System.out.println("");
        selecionarGatitoMenosMovi();

        if (exito == true && FrontEnd.juegoTerminado == false) {
            turnoVerificarSiGanoCuadrito();
            Timer t = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    turnoComputadora();

                }
            };
            t.schedule(task, 1000);
        }
    }

    /**
     * Aqui no podemos usar la fácilidad del evento entonces harcodeamos todo
     *
     * @param c
     */
    private void turnoComputadora() {

        System.out.println("PC");
        c = FrontEnd.turno == true ? 'X' : 'O';

        FrontEnd.cuadro[FrontEnd.idF].backEndGato.turnoComputadora();

        x = FrontEnd.cuadro[FrontEnd.idF].backEndGato.getX();
        y = FrontEnd.cuadro[FrontEnd.idF].backEndGato.getY();

        /*
        *The most annoying bug in the game fixed already
         */
        FrontEnd.cuadro[FrontEnd.idF].cuadritos[x][y].setText("" + c);
        FrontEnd.frmLogCat.setCoords((byte) FrontEnd.idF, x, y, c);

        turnoVerificarSiGanoCuadritoPC();

        //select the square where to play
        FrontEnd.idF = cuadroDondeJugar.selecionarId(x, y);
        FrontEnd.seleccionarId(true);
        selecionarGatitoMenosMovi();
        FrontEnd.isWinner();

    }

    private void playerOnline() {
        JOptionPane.showMessageDialog(null, "WIP", "ERROR", JOptionPane.ERROR_MESSAGE);
    }

    private void turnoVerificarSiGanoCuadrito() {
        if (FrontEnd.turno == true) {//turno jugador
            FrontEnd.turno = false;
            gatitoGanado = backEndGato.isWinner(1);
            dibujarCuadritos('X');
        } else {//turno del segundo jugador
            FrontEnd.turno = true;
            System.out.println("pccc");
            gatitoGanado = backEndGato.isWinner(2);
            dibujarCuadritos('O');
        }
    }

    private void turnoVerificarSiGanoCuadritoPC() {
        FrontEnd.turno = true;
        System.out.println("pccc");
        gatitoGanado = FrontEnd.cuadro[FrontEnd.idF].backEndGato.isWinner(2);
        if (gatitoGanado == true) {
            for (i = 0; i < 3; i++) {
                for (j = 0; j < 3; j++) {
                    FrontEnd.cuadro[FrontEnd.idF].cuadritos[i][j].setText("" + c);
                }
            }
        }

    }

    private void dibujarCuadritos(char c) {
        if (gatitoGanado == true) {
            for (i = 0; i < 3; i++) {
                for (j = 0; j < 3; j++) {
                    cuadritos[i][j].setText("" + c);
                }
            }
        }
    }

    /**
     * Cuando en el gatito has ganado debes moverte.(Leer con voz se Yoda, el
     * tipo verde de star wars) entonces debes seleccionar el que tenga menos
     * movimientos jugados
     */
    private void selecionarGatitoMenosMovi() {
        if (FrontEnd.cuadro[FrontEnd.idF].gatitoGanado) {
            byte menor = 9;
            for (byte i = 0; i < 9; i++) {
                if (FrontEnd.cuadro[i].backEndGato.movimientos < menor && FrontEnd.cuadro[i].backEndGato.winner == false) {
                    menor = FrontEnd.cuadro[i].backEndGato.movimientos;
                    FrontEnd.idF = i;
                }
            }
            FrontEnd.seleccionarId(true);
        }
    }

    private byte tipoDeJuego;

    void nuevoJuego() {

        System.out.println("Tipo de juego" + FrontEnd.tipoDeJuego);
        tipoDeJuego = FrontEnd.tipoDeJuego;
        FrontEnd.primeraVez = tipoDeJuego == 0 ? true : false;
        FrontEnd.frmChat.cleanChat();
        FrontEnd.primeraVez = true;
        FrontEnd.turno = true;
        FrontEnd.juegoTerminado = false;
        FrontEnd.seleccionarId(false);
        backEndGato.newGame();
        gatitoGanado = false;
        FrontEnd.frmLogCat.clearLogGame();

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                cuadritos[i][j].setText(" ");
            }
        }
        //Lets clean all unecessary things
        System.gc();
    }

}
