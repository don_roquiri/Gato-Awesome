package model;

import java.io.Serializable;

/**
 *
 * @author str4ng3r
 */
public class MessagePackage implements Serializable{
    private String userName;
    private String IP;
    private String message;
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }  
   
}
