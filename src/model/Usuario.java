/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author str4ng3r
 */
public class Usuario implements Serializable {

    public static Usuario USER = new Usuario();
    private String userName;
    private String email;
    private String password;
    private String IP;
    private int countWinner;
    private int countLoss;

    public Usuario() {
    }

    public Usuario(String userName, String email, String password, String IP, int countWinner, int countLoss) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.IP = IP;
        this.countWinner = countWinner;
        this.countLoss = countLoss;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public int getCountWinner() {
        return countWinner;
    }

    public void setCountWinner(int countWinner) {
        this.countWinner = countWinner;
    }

    public int getCountLoss() {
        return countLoss;
    }

    public void setCountLoss(int countLoss) {
        this.countLoss = countLoss;
    }

}
