/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import baseData.*;
import gato.FrontEnd;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author str4ng3r
 */
public class Server implements CommonVars{

    public Server() {
        try {
            ServerSocket servidor = new ServerSocket(PORT);
            Socket newClient = servidor.accept();
            ObjectInputStream turn = new ObjectInputStream(newClient.getInputStream());//Server obtiene el turno 
            
            System.out.println(turn.readBoolean());
            ObjectOutputStream answer = new ObjectOutputStream(newClient.getOutputStream());
            answer.writeBoolean(FrontEnd.turno);
            newClient.close();
            servidor.close();
            //turno cambiado
            
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null , ex);
        }
        
    }
    
}
