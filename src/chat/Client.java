/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import baseData.*;
import gato.FrontEnd;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;

/**
 *
 * @author str4ng3r
 */


public class Client implements CommonVars {

    public Client() {
        try {
            Socket client = new Socket(Usuario.USER.getIP(), PORT);
            ObjectOutputStream mensaje = new ObjectOutputStream(client.getOutputStream());
            mensaje.writeBoolean(FrontEnd.turno);//Client envia turno al Server

            ObjectInputStream entrada = new ObjectInputStream(client.getInputStream());
            entrada.readBoolean();
            client.close();
            System.out.println("Conexión Cerrada");
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "CLIENT ERROR", e);
        }

    }
    
}
