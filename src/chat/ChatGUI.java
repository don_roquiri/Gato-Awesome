package chat;

import baseData.RegisterLogin;
import baseData.Client;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import model.Usuario;

public class ChatGUI extends JPanel implements ActionListener{

    JButton     sendMessage;
    JTextField  messageBox;
    static JTextArea   chatBox;

    String message = "";
    
    public ChatGUI() {
        super(null);
        display();
    }

    public void display() {
        //Utilizar el borderLayout
        setLayout(new BorderLayout());
        //Creamos una panel que estara en el sur y le ponemos color a lo whats app
        JPanel southPanel = new JPanel();
        setBackground(new Color(9 , 210, 97));
        southPanel.setBackground(new Color(9 , 210, 97));//whatsApp color extraido
        southPanel.setLayout(new GridBagLayout());
        messageBox = new JTextField(10);
        messageBox.requestFocusInWindow();
        
        //botonEnviarMensaje
        sendMessage = new JButton();
        ImageIcon icon = new ImageIcon(getClass().getResource("../Drawables/arrow.png"));
       
        //haciendo el boton Flat
        sendMessage.setIcon(icon);
        sendMessage.addActionListener(this);
        sendMessage.setBorderPainted(false);
        sendMessage.setFocusPainted(false);
        sendMessage.setContentAreaFilled(false);

        chatBox = new JTextArea();
        chatBox.setEditable(false);
        chatBox.setLineWrap(true);
        //agregamos la caja de mensaejes de LADO IZQUIERDO
        
        GridBagConstraints config = new GridBagConstraints();
        //Insets crea un pequeño espacio entre componentes
        config.insets = new Insets(3, 3, 3, 3);
        config.anchor = GridBagConstraints.LINE_START;
        config.fill = GridBagConstraints.HORIZONTAL;
        config.weightx = 256.0D;
        config.weighty = 1.0D;
        southPanel.add(messageBox, config);
        //Insets crea un pequeño espacio entre componentes
        //Añadimos boton de lado derecho
        config.anchor = GridBagConstraints.LINE_END;
        config.fill = GridBagConstraints.NONE;
        config.weightx = 1.0D;
        config.weighty = 1.0D;
        southPanel.add(sendMessage, config);
        
        //Añadimos la caja del chat
        add(new JScrollPane(chatBox), BorderLayout.CENTER);
        //Añadimos la caja sur
        add(southPanel, BorderLayout.SOUTH);
        
        //Borde
        TitledBorder border = new TitledBorder("Chat Gato");
        border.setTitleJustification(TitledBorder.CENTER);
        border.setTitlePosition(TitledBorder.TOP);
        setBorder(border);

    }
   
    private void emparejamiento(){
        /**
         * [WIP] Base Date metodo que empareje los usuarios
         * para chatear y jugar en linea
         */
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==sendMessage){
            if(RegisterLogin.CONEXION){
                message = "[" + Usuario.USER.getUserName() + "]: "+ messageBox.getText() + "\n";
                Client.outMessage(message, Usuario.USER.getUserName(), Usuario.USER.getIP());
                passwordSHA256(message);
            }else{
                message = "[ERROR]: " + messageBox.getText() + "\n";
                JOptionPane.showMessageDialog(null, "Necesitas logearte parar enviar mensajes y seleccionar un usuario entre todos");
            }
            chatBox.append(message);
            messageBox.setText("");
        }
    }
        
    private String passwordSHA256(String password) {
            MessageDigest md = null;
            try {
                    md = MessageDigest.getInstance("SHA-256");
            } 
            catch (NoSuchAlgorithmException e) {		
                    e.printStackTrace(); 
                    return null;
            }

            byte[] hash = md.digest(password.getBytes());
            StringBuffer sb = new StringBuffer();

            for(byte b : hash) {        
                    sb.append(String.format("%02x", b));
            }
                System.out.println("Message on SHA256: " + sb.toString());
            return sb.toString();
    }   
    
    public static void getMessage(String userName, String message){
            chatBox.append("[" + userName + "]: "+ message + "\n");   
    }
    
    public void cleanChat(){
        message = "";
        chatBox.setText("");
    }
}